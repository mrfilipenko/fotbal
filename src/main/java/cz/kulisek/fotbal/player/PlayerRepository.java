package cz.kulisek.fotbal.player;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long>{

	public Optional<List<Player>> findAllByFirstNameAndLastName(String firstName, String lastName);	
	public Optional<List<Player>> findAllByTeamId(long id);
}
