package cz.kulisek.fotbal.player.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import cz.kulisek.fotbal.exception.ResourceNotFoundException;
import cz.kulisek.fotbal.player.Player;
import cz.kulisek.fotbal.player.PlayerRepository;
import cz.kulisek.fotbal.player.PlayerService;
import cz.kulisek.fotbal.team.TeamRepository;

@Service
public class PlayerServiceImpl implements PlayerService{
	
	@Autowired
	PlayerRepository playerRepository;
	
	@Autowired
	TeamRepository teamRepository;
	
	@Override
	public List<Player> getByName(String firstName, String lastName) {
		return playerRepository.findAllByFirstNameAndLastName(firstName, lastName)
					.orElseThrow(() -> new ResourceNotFoundException("Player with name " + firstName + " " + lastName + " not found."));
	}

	@Override
	public Player createPlayer(long teamId, Player player) {
		return teamRepository.findById(teamId).map(team -> {
			player.setTeam(team);
			return playerRepository.save(player);
		}).orElseThrow(() -> new ResourceNotFoundException("TeamId " + teamId + " not found."));
	}

	@Override
	public Player updatePlayer(long teamId, long playerId, Player playerRequest) {
		if(!teamRepository.existsById(teamId)) {
			throw new ResourceNotFoundException("TeamId " + teamId + " not found.");
		}
		
		return playerRepository.findById(playerId).map(player -> {
			player.setFirstName(playerRequest.getFirstName());
			player.setLastName(playerRequest.getLastName());
			player.setPosition(playerRequest.getPosition());
			player.setActive(playerRequest.isActive());
			
			return playerRepository.save(player);
		}).orElseThrow(() -> new ResourceNotFoundException("PlayerId " + playerId + " not found."));
		
	}
	
	@Override
	public ResponseEntity<?> deletePlayer(long teamId, long playerId) {
		if(!teamRepository.existsById(teamId)) {
			throw new ResourceNotFoundException("TeamId " + teamId + " not found.");
		}
		
		return playerRepository.findById(playerId).map(player -> {
			playerRepository.delete(player);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("PlayerId " + playerId + " not found."));
	}
}
