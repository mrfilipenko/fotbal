package cz.kulisek.fotbal.player;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PlayerController {
	
	@Autowired
	PlayerService playerService;
	
	@GetMapping("/public/players/{firstName}/{lastName}")
	public List<Player> getByName(@PathVariable String firstName,
							@PathVariable String lastName) {
		return playerService.getByName(firstName, lastName);
	}
	
	@PostMapping("/private/teams/{teamId}/players")
	public Player createPlayer(@PathVariable long teamId,
							   @Valid @RequestBody Player player) {
		return playerService.createPlayer(teamId, player);
	}
	
	@PutMapping("/private/teams/{teamId}/players/{id}")
	public Player updatePlayer(@PathVariable (value = "teamId") long teamId,
							   @PathVariable (value = "id") long playerId,
							   @Valid @RequestBody Player playerRequest) {
		return playerService.updatePlayer(teamId, playerId, playerRequest);
	}
	
	@DeleteMapping("/private/teams/{teamId}/players/{playerId}")
	public ResponseEntity<?> deletePlayer(@PathVariable (value = "teamId") long teamId,
							 @PathVariable (value = "playerId") long playerId) {
		
		return playerService.deletePlayer(teamId, playerId);
	}
	
}
