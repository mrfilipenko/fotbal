package cz.kulisek.fotbal.player;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface PlayerService {
	
	public List<Player> getByName(String firstName, String lastName);
	public Player createPlayer(long teamId, Player player);
	public Player updatePlayer(long teanId, long playerId, Player playerRequest);
	public ResponseEntity<?> deletePlayer(long teamId, long playerId);
}
