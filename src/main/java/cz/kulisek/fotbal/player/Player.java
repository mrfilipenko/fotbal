package cz.kulisek.fotbal.player;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import cz.kulisek.fotbal.team.Team;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Table (name = "players")
public class Player {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	@Column(name = "id", columnDefinition = "serial", unique = true, nullable = false)
	private long id;
	
	@Getter
	@Setter
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "team_id", nullable = true)	
	private Team team;
	
	@Getter
	@Setter
	@Column(name = "first_name", nullable = false)
	private String firstName;
	
	@Getter
	@Setter
	@Column(name = "last_name", nullable = false)
	private String lastName;
	
	@Getter
	@Setter
	@Column(name = "position", nullable = false)
	private String position;
	
	@Getter
	@Setter
	@Column(name = "active", nullable = false)
	private boolean active; // zakladni sestava
}
