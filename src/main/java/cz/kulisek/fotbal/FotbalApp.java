package cz.kulisek.fotbal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FotbalApp {

	public static void main(String[] args) {
		SpringApplication.run(FotbalApp.class, args);
	}
}
