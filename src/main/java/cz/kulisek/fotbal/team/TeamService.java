package cz.kulisek.fotbal.team;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface TeamService {
	
	public Page<Team> getAllTeamsPrivate(Pageable pageable);
	public Page<Team> getAllTeamsPublic(Pageable pageable);
	public Team createTeam(Team team);
	public Team updateTeam(long teamId, Team teamRequest);
	public ResponseEntity<?> deleteTeam(long teamId);
	public Team getByName(String name);
}
