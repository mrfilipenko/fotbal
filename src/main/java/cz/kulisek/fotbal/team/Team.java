package cz.kulisek.fotbal.team;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import cz.kulisek.fotbal.player.Player;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Table(name = "teams")
public class Team {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "serial", name = "id", unique = true, nullable = false)
	@Getter
	private long id;
	
	@Column(name = "name", unique = true, nullable = false)
	@Getter
	@Setter
	private String name;
	
	@Column(name = "description", nullable = false)
	@Getter
	@Setter
	private String description;
	
	@Getter
	@OneToMany(cascade = CascadeType.ALL,
			   mappedBy = "team")
	@JsonManagedReference
	private List<Player> players;
}
