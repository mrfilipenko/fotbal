package cz.kulisek.fotbal.team.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import cz.kulisek.fotbal.exception.ResourceNotFoundException;
import cz.kulisek.fotbal.player.Player;
import cz.kulisek.fotbal.player.PlayerRepository;
import cz.kulisek.fotbal.team.Team;
import cz.kulisek.fotbal.team.TeamRepository;
import cz.kulisek.fotbal.team.TeamService;

@Service
public class TeamServiceImpl implements TeamService {
	
	@Autowired
	TeamRepository teamRepository;
	
	@Autowired
	PlayerRepository playerRepository;
	
	public Team getByName(String name) {
		return teamRepository.findByName(name)
				.orElseThrow(() -> new ResourceNotFoundException("TeamName " + name + " not found."));
	}
	
	public void create(Team team) {
		teamRepository.save(team);
	}

	public Page<Team> getAllTeamsPrivate(Pageable pageable) {
		return teamRepository.findAll(pageable);
	}
	
	public Page<Team> getAllTeamsPublic(Pageable pageable) {
		Page<Team> page = teamRepository.findTeams(pageable);
		List<Team> teams = page.getContent();
		
		for(Team t: teams) {
			List<Player> toRemove = new ArrayList<Player>();
			
			t.getPlayers().stream()
				.filter(player -> player.isActive() == false)
				.forEach(player ->
					toRemove.add(player)
				);
			
			if(!toRemove.isEmpty()) {
				t.getPlayers().removeAll(toRemove);
				toRemove.clear();
			}
		}

		return page;
	}
	
	public Team createTeam(Team team) {
		return teamRepository.save(team);
	}
	
	public Team updateTeam(long teamId, Team teamRequest) {
		return teamRepository.findById(teamId).map(team -> {
			team.setName(teamRequest.getName());
			team.setDescription(teamRequest.getDescription());
			return teamRepository.save(team);
		}).orElseThrow(() -> new ResourceNotFoundException("TeamId " + teamId + " not found."));
		
	}
	
	public ResponseEntity<?> deleteTeam(long teamId) {
		return teamRepository.findById(teamId).map(team -> {
			Optional<List<Player>> playersToDelete = playerRepository.findAllByTeamId(teamId);
			
			if(playersToDelete.isPresent()) {
				playersToDelete.get().stream()
					.forEach(player -> 
						playerRepository.delete(player)
					);
			}
			
			teamRepository.delete(team);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("TeamId " + teamId + " not found."));
	}
}
