package cz.kulisek.fotbal.team;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {
	
	public Optional<Team> findByName(String name);
	
	@Query("Select t from Team t WHERE EXISTS(SELECT p FROM t.players p WHERE p.active = true)")
	public Page<Team> findTeams(Pageable pageable);
}
