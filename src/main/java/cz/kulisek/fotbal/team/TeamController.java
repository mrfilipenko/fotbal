package cz.kulisek.fotbal.team;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class TeamController {

	@Autowired
	TeamService teamService;
	
	@GetMapping("/public/teams/{name}")
	public Team getTeam(@PathVariable String name) {
		return teamService.getByName(name);
	}

	@GetMapping("/public/teams")
	public Page<Team> getAllTeamsPublic(@PageableDefault(value = 5, page = 0) Pageable pageable) {
		return teamService.getAllTeamsPublic(pageable);
	}
	
	@GetMapping("/private/teams")
	public Page<Team> getAllTeamsPrivate(@PageableDefault(value = 5, page = 0) Pageable pageable) {
		return teamService.getAllTeamsPrivate(pageable);
	}
	
	@PostMapping("/private/teams")
	public Team createTeam(@Valid @RequestBody Team team) {
		return teamService.createTeam(team);
	}
	
	@PutMapping("/private/teams/{teamId}")
	public Team updateTeam(@PathVariable long teamId,
						   @Valid @RequestBody Team teamRequest) {
		return teamService.updateTeam(teamId, teamRequest);
	}
	
	@DeleteMapping("/private/teams/{teamId}")
	public ResponseEntity<?> deleteComment(@PathVariable long teamId) {
		return teamService.deleteTeam(teamId);
	}
	
}
