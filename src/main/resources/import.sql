INSERT INTO teams (name, description) VALUES ('Manchester United', 'Popis');
INSERT INTO teams (name, description) VALUES ('Arsenal FC', 'Popis');
INSERT INTO teams (name, description) VALUES ('Real Madrid', 'Popis');
INSERT INTO teams (name, description) VALUES ('FC Barcelona', 'Popis');
INSERT INTO teams (name, description) VALUES ('Bayern Munich', 'Popis');
INSERT INTO teams (name, description) VALUES ('Manchester City ', 'Popis');

INSERT INTO players (first_name, last_name, position, active, team_id) VALUES('Lionel', 'Messi', 'Forward', true, '1');
INSERT INTO players (first_name, last_name, position, active, team_id) VALUES('Christiano', 'Ronaldo', 'Midfielder', true, '1');
INSERT INTO players (first_name, last_name, position, active, team_id) VALUES('Luka', 'Modric', 'Defender', false, '1');
INSERT INTO players (first_name, last_name, position, active, team_id) VALUES('Zlatan', 'Ibrahimovic', 'Forward', false, '1');
INSERT INTO players (first_name, last_name, position, active, team_id) VALUES('Kylian', 'Mbappe', 'Forward', true, '1');
INSERT INTO players (first_name, last_name, position, active, team_id) VALUES('Thierry', 'Henry', 'Defender', true, '2');
INSERT INTO players (first_name, last_name, position, active, team_id) VALUES('Paul', 'Pobga', 'Defender', false, '2');
INSERT INTO players (first_name, last_name, position, active, team_id) VALUES('Mohamed', 'Salah', 'Midfielder', true, '3');
INSERT INTO players (first_name, last_name, position, active, team_id) VALUES('Roberto', 'Baggio', 'Defender', false, '3');
INSERT INTO players (first_name, last_name, position, active, team_id) VALUES('David', 'Silva', 'Defender', true, '4');
INSERT INTO players (first_name, last_name, position, active, team_id) VALUES('Alex', 'Ferguson', 'Midfielder', false, '4');